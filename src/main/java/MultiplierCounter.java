import java.util.ArrayList;

/**
 * Created by Marcin on 2017-01-09.
 */
public class MultiplierCounter {

    private ArrayList<Multiplier> multiplierList;

    public MultiplierCounter() {
        multiplierList = new ArrayList<>();
    }

    public int addPoints(BoardField boardField) {
        int points = boardField.letter.getPoints();

        if(boardField.multiplier == Multiplier.LETTER2x) {
            points *= 2;
        }
        if(boardField.multiplier == Multiplier.LETTER3x) {
            points *= 3;
        }

        return points;
    }

    public void addPossibleMultipliers(BoardField boardField) {
        if(boardField.multiplier == Multiplier.WORD2x) {
            multiplierList.add(Multiplier.WORD2x);
        }
        if(boardField.multiplier == Multiplier.WORD3x) {
            multiplierList.add(Multiplier.WORD3x);
        }
    }

    public void removeMultipliers() {
        multiplierList.clear();
    }

    public int getWordMultiplier() {
        int multiplier = 1;
        for (Multiplier m : multiplierList) {
            if(m==Multiplier.WORD2x) {
                multiplier *= 2;
            }
            if(m==Multiplier.WORD3x) {
                multiplier *= 3;
            }
        }
        return multiplier;
    }
}
