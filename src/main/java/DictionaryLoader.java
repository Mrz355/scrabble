import javafx.application.Platform;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Marcin on 2016-12-26.
 */

//Class that checks the lexical correction of the word
public class DictionaryLoader implements Runnable {
    private List<String> list;
    private final String dictionaryPath = "src\\main\\resources\\words.txt";
    private boolean dictionaryLoaded = false;   // flag that indicates the state of the worker, dicionary-loader thread
    private Game game;

    public DictionaryLoader(Game game) {
        this.game = game;
        list = new CopyOnWriteArrayList<>(); // CopyOnWriteArrayLists support multithreading

        new Thread(this,"DictionaryLoader").start(); // We load the list in the other thread. We don't want to suspend the main thread
    }
    public boolean hasWord(String s) {
        if (list.contains(s)) return true;
        return false;
    }

    @Override
    public void run() {
        try (Stream<String> stream = Files.lines(Paths.get(dictionaryPath))) {
            list = stream.collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        dictionaryLoaded = true;
        // If game displayed the waiting pane, we have to invoke the method from main thread to hide it back
        if (game.isWaiting()) {
            Runnable runnable = () -> game.hideWaitingPane();
            Platform.runLater(runnable);
        }
    }

    public boolean isDictionaryLoaded() {
        return dictionaryLoaded;
    }
}
