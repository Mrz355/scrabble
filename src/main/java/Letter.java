/**
 * Created by Marcin on 2016-12-26.
 */
public class Letter {
    private int points;
    private char letter;

    public int getPoints() {
        return points;
    }

    public char getLetter() {
        return letter;
    }

    public Letter(char letter) {
        this.letter = letter;
        this.points = pointsFor(letter);
    }

    //Generate fixed points for every letter
    static int pointsFor(char letter) {
        switch (letter) {
            case 'A':
                return 1;
            case 'Ą':
                return 5;
            case 'B':
                return 3;
            case 'C':
                return 2;
            case 'Ć':
                return 6;
            case 'D':
                return 2;
            case 'E':
                return 1;
            case 'Ę':
                return 5;
            case 'F':
                return 5;
            case 'G':
                return 3;
            case 'H':
                return 3;
            case 'I':
                return 1;
            case 'J':
                return 3;
            case 'K':
                return 2;
            case 'L':
                return 2;
            case 'Ł':
                return 3;
            case 'M':
                return 2;
            case 'N':
                return 1;
            case 'Ń':
                return 7;
            case 'O':
                return 1;
            case 'Ó':
                return 5;
            case 'P':
                return 2;
            case 'R':
                return 1;
            case 'S':
                return 1;
            case 'Ś':
                return 5;
            case 'T':
                return 2;
            case 'U':
                return 3;
            case 'W':
                return 1;
            case 'Y':
                return 2;
            case 'Z':
                return 1;
            case 'Ź':
                return 9;
            case 'Ż':
                return 5;
            default:
                return 0;
        }
    }
}
