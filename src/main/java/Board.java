import java.util.ArrayList;

/**
 * Created by Marcin on 2016-12-29.
 */
public class Board {

    private BoardField[][] board;
    private int points;

    public Board() {
        board = new BoardField[15][15];
        initializeBoardFields();
    }

    private void initializeBoardFields() {
        for(int i=0;i<15;++i) {
            board[i] = new BoardField[15];
            for(int j=0;j<15;++j) {
                board[i][j] = new BoardField();
                board[i][j].isPlaceAble = false;
                board[i][j].isPlaced = false;
            }
        }
        board[7][7].isPlaceAble = true;

        //Initialize multipliers
        for(int i=0;i<15;++i) for(int j=0;j<15;++j) board[0][0].multiplier = null;  // Make sure that not assigned multipliers are nulls

        board[0][0].multiplier = Multiplier.WORD3x;
        board[0][7].multiplier = Multiplier.WORD3x;
        board[0][14].multiplier = Multiplier.WORD3x;
        board[7][0].multiplier = Multiplier.WORD3x;
        board[7][14].multiplier = Multiplier.WORD3x;
        board[14][0].multiplier = Multiplier.WORD3x;
        board[14][7].multiplier = Multiplier.WORD3x;
        board[14][14].multiplier = Multiplier.WORD3x;

        board[1][1].multiplier = Multiplier.WORD2x;
        board[2][2].multiplier = Multiplier.WORD2x;
        board[3][3].multiplier = Multiplier.WORD2x;
        board[3][3].multiplier = Multiplier.WORD2x;

        board[1][13].multiplier = Multiplier.WORD2x;
        board[2][12].multiplier = Multiplier.WORD2x;
        board[3][11].multiplier = Multiplier.WORD2x;
        board[4][10].multiplier = Multiplier.WORD2x;

        board[13][1].multiplier = Multiplier.WORD2x;
        board[12][2].multiplier = Multiplier.WORD2x;
        board[11][3].multiplier = Multiplier.WORD2x;
        board[10][4].multiplier = Multiplier.WORD2x;

        board[13][13].multiplier = Multiplier.WORD2x;
        board[12][12].multiplier = Multiplier.WORD2x;
        board[11][11].multiplier = Multiplier.WORD2x;
        board[10][10].multiplier = Multiplier.WORD2x;

        board[1][5].multiplier = Multiplier.LETTER3x;
        board[1][9].multiplier = Multiplier.LETTER3x;


        board[5][1].multiplier = Multiplier.LETTER3x;
        board[5][5].multiplier = Multiplier.LETTER3x;
        board[5][9].multiplier = Multiplier.LETTER3x;
        board[5][13].multiplier = Multiplier.LETTER3x;

        board[9][1].multiplier = Multiplier.LETTER3x;
        board[9][5].multiplier = Multiplier.LETTER3x;
        board[9][9].multiplier = Multiplier.LETTER3x;
        board[9][13].multiplier = Multiplier.LETTER3x;

        board[13][5].multiplier = Multiplier.LETTER3x;
        board[13][9].multiplier = Multiplier.LETTER3x;

        board[0][3].multiplier = Multiplier.LETTER2x;
        board[0][11].multiplier = Multiplier.LETTER2x;

        board[14][3].multiplier = Multiplier.LETTER2x;
        board[14][11].multiplier = Multiplier.LETTER2x;

        board[3][0].multiplier = Multiplier.LETTER2x;
        board[11][0].multiplier = Multiplier.LETTER2x;

        board[3][14].multiplier = Multiplier.LETTER2x;
        board[11][14].multiplier = Multiplier.LETTER2x;

        board[2][6].multiplier = Multiplier.LETTER2x;
        board[2][8].multiplier = Multiplier.LETTER2x;
        board[3][7].multiplier = Multiplier.LETTER2x;

        board[12][6].multiplier = Multiplier.LETTER2x;
        board[12][8].multiplier = Multiplier.LETTER2x;
        board[11][7].multiplier = Multiplier.LETTER2x;

        board[6][2].multiplier = Multiplier.LETTER2x;
        board[7][3].multiplier = Multiplier.LETTER2x;
        board[8][2].multiplier = Multiplier.LETTER2x;

        board[6][12].multiplier = Multiplier.LETTER2x;
        board[7][11].multiplier = Multiplier.LETTER2x;
        board[8][12].multiplier = Multiplier.LETTER2x;

        board[6][6].multiplier = Multiplier.LETTER2x;
        board[8][8].multiplier = Multiplier.LETTER2x;
        board[8][6].multiplier = Multiplier.LETTER2x;
        board[6][8].multiplier = Multiplier.LETTER2x;
    }

    // Main logic of the game. Checking if word can be placed in that correlation. Took me hours to finally made it to the end.
    public boolean isGood(char[][] newWord) {
        boolean isPlaceAble = false;
        boolean firstLetter = true;
        boolean sameRowAndCol = true;
        boolean areNextToEachOther = true;
        boolean columnWord = false;
        boolean rowWord = false;
        boolean twoLetters = false;
        boolean endOfTheWord = false;
        int row=-1,col=-1;
        for(int i=0;i<15;++i) {
            if(twoLetters) break;
            for(int j=0;j<15;++j) {
                if(newWord[i][j]!='\u0000') {
                    if(firstLetter) {
                        row = i;
                        col = j;
                        firstLetter = false;
                    } else {
                        twoLetters = true;
                        if(i==row) {
                            rowWord = true;
                        } else if (j==col) {
                            columnWord = true;
                        } else {
                            areNextToEachOther = false;
                        }
                        break;
                    }
                }
            }
        }
        int counter =0;
        // I check the number of letters
        for(int i=0;i<15;++i) {
            for(int j=0;j<15;++j) {
                if(newWord[i][j]!='\u0000' ) {
                    counter++;
                }
            }
        }
        int actual_counter = 0;
        if(rowWord) { // row = const
            for(int j=col;j<15;++j) {
                if(newWord[row][j]=='\u0000' && !board[row][j].isPlaced) {
                    endOfTheWord = true;
                } else {
                    if(endOfTheWord) {
                        areNextToEachOther = false;
                        break;
                    }
                    if(board[row][j].isPlaceAble) {
                        isPlaceAble = true;
                    }
                }
                if(newWord[row][j]!='\u0000') {
                    actual_counter++;
                }
            }
        } else if (columnWord) {
            for(int i=row;i<15;++i) {
                if(newWord[i][col]=='\u0000' && !board[i][col].isPlaced) {
                    endOfTheWord = true;
                } else {
                    if(endOfTheWord) {
                        areNextToEachOther = false;
                        break;
                    }
                    if(board[i][col].isPlaceAble) {
                        isPlaceAble = true;
                    }
                }
                if(newWord[i][col]!='\u0000') {
                    actual_counter++;
                }
            }
        } else if(!firstLetter) {
            if(board[row][col].isPlaceAble) {
                isPlaceAble = true;
            }
            actual_counter++;
        }
        return sameRowAndCol && isPlaceAble && areNextToEachOther && actual_counter == counter;
    }

    public int getPoints() {
        int result = points;
        points = 0;
        return result;
    }

    public void wordPlaced(char[][] newWord) {
        for(int i=0;i<15;++i) {
            for(int j=0;j<15;++j) {
                if(newWord[i][j]!='\u0000') {
                    setPlaceability(i,j,true);
                    board[i][j].isPlaced = true;
                    board[i][j].letter = new Letter(newWord[i][j]);
                }
            }
        }

    }

    private void setPlaceability(int i, int j, boolean isPlaceAble) {
        if(i-1>=0) board[i-1][j].isPlaceAble = isPlaceAble;
        if(i+1<15) board[i+1][j].isPlaceAble = isPlaceAble;
        if(j-1>=0) board[i][j-1].isPlaceAble = isPlaceAble;
        if(j+1<15) board[i][j+1].isPlaceAble = isPlaceAble;
        board[7][7].isPlaceAble=true;
    }

    // Getting the words placed on the board (simultaneously we get points)
    public ArrayList<String> getWords(char[][] newWord) {
        //Checking words
        //Checking vertical words
        // (it is really easier to first check the vertical and then horizontal words in this method, instead of splitting it to other and I think it is more readable as well)
        ArrayList<String> words = new ArrayList<>();
        points = 0;
        MultiplierCounter multiplierCounter = new MultiplierCounter();
        boolean newActualWord = false;
        for(int j=0;j<15;++j) {
            String actual_word = "";
            int tmp_points = 0;
            newActualWord = false;
            for(int i=0;i<15;++i){
                if(board[i][j].isPlaced) {
                    actual_word+=board[i][j].letter.getLetter();
                    tmp_points += multiplierCounter.addPoints(board[i][j]);
                    multiplierCounter.addPossibleMultipliers(board[i][j]);
                }
                if(newWord[i][j]!='\u0000') {
                    newActualWord = true;
                }
                if(!board[i][j].isPlaced) {
                    if(newActualWord) {
                        if(actual_word.length()>1) {
                            words.add(actual_word);
                            tmp_points*=multiplierCounter.getWordMultiplier();
                            points += tmp_points;
                        }
                    }
                    actual_word="";
                    multiplierCounter.removeMultipliers();
                    tmp_points = 0;
                }
            }
        }
        //Checking horizontal words
        for(int i=0;i<15;++i) {
            String actual_word="";
            int tmp_points = 0;
            newActualWord = false;
            for(int j=0;j<15;++j) {
                if(board[i][j].isPlaced) {
                    actual_word+=board[i][j].letter.getLetter();
                    tmp_points += multiplierCounter.addPoints(board[i][j]);
                    multiplierCounter.addPossibleMultipliers(board[i][j]);
                }
                if(newWord[i][j]!='\u0000') {
                    newActualWord = true;
                }
                if(!board[i][j].isPlaced) {
                    if(newActualWord) {
                        if(actual_word.length()>1) {
                            words.add(actual_word);
                            tmp_points*=multiplierCounter.getWordMultiplier();
                            points += tmp_points;
                        }
                    }
                    actual_word="";
                    multiplierCounter.removeMultipliers();
                    tmp_points = 0;
                }
            }
        }
        return words;
    }

    public void wrongWord(char[][] activeWord) {
        for(int i=0;i<15;++i) {
            for(int j=0;j<15;++j) {
                if(activeWord[i][j]!='\u0000') {
                    setPlaceability(i,j,false);
                    board[i][j].isPlaced = false;
                }
            }
        }
    }

    public BoardField[][] getBoard() {
        return board;
    }
}
