import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

public class Controller {
    @FXML
    public HBox letterBox;
    @FXML
    public Label playerLabel;
    @FXML
    public Button exchangeButton;
    @FXML
    public Label playerLabel2;
    @FXML
    public Pane tourPane;
    @FXML
    public Button okButton2;
    @FXML
    public GridPane gridRanking;
    @FXML
    public Pane playerPane;
    @FXML
    public Button playerButton;
    @FXML
    public TextField playerTextField;
    @FXML
    public Label menuPlayerLabel;

    public Pane getPlayerPane() {
        return playerPane;
    }

    public Button getPlayerButton() {
        return playerButton;
    }

    public TextField getPlayerTextField() {
        return playerTextField;
    }

    @FXML
    private Button okButton;
    @FXML
    private GridPane gridPane1;

    public GridPane getGridPane1() {
        return gridPane1;
    }
    public HBox getLetterBox() {
        return letterBox;
    }
    @FXML
    private void initialize() {
    }

    public Button getOkButton() {
        return okButton;
    }
    public Label getPlayerLabel() {
        return playerLabel;
    }
    public Button getExchangeButton() {
        return exchangeButton;
    }
    public Pane getTourPane() {
        return tourPane;
    }
    public Button getTourButton() {
        return okButton2;
    }
    public Label getTourLabel() {
        return playerLabel2;
    }
    public Label getMenuPlayerLabel() {
        return menuPlayerLabel;
    }

    public GridPane getGridRanking() {
        return gridRanking;
    }
}
