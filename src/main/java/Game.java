import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;

import java.util.ArrayList;

/**
 * Created by Marcin on 2016-12-27.
 */
public class Game {

    private GridPane gridPane; // Scrabble board container
    private Point lastPressed,lastPressed2; // Holds last pressed button(s) position on board
    private Button[][] buttonTab; // Buttons tab to store on board buttons
    private HBox letterBox; // Container of the players' letters
    private Button[] lettersButtonTab; // Buttons tab to store players' letters
    private int letter1; // Index of clicked letter in players' letters
    private boolean isLetterClicked = false; // Boolean flag that indicates if one of players' letters (buttons) were clicked
    private boolean isBoardClicked = false; // Boolean flag that indicates if one of board's buttons were clicked
    private Button b1, b2; // Clicked buttons whose will exchange letters with each other
    private Controller controller; // JavaFX forms manager
    private Board board; // 15x15 Board of the scrabble. Used to manage the words placed on board (for eg. checking the correction of placed buttons - must be in one line)
    private Button okButton; // Button clicked by user to accept by him the placed word and end his tour (After ExchangeLetters being clicked it is transformed to Accept exchange button)
    private char[][] activeWord; // Array containing the letters placed by user in this exact turn
    private int players; // The number of players in the game
    private Player[] player; // Array holding the players
    private int activePlayer; // The index of an active player, which now has the turn
    private Bag bag; // The bag of letters. We can get letters from here and measure points for word
    private ArrayList<String> words; // The words that user created in his turn (in Scrabble, player can place more than one word, by tricky placement of letters)
    private Label playerLabel; // This is the label that indicates the current player
    private Button exchangeButton; // Button that turn the exchanging letters mode on. It is after used as cancel exchange button
    private Pane tourPane; // Popup Pane that occurs when the player end his turn (contains sum up info about his turn and displays who's the next player)
    private Button tourButton; // Ok button on the turn popup pane (it close it or display the next label)
    private Label tourLabel; // The label which display informamtions on tourPane
    private DictionaryLoader dictionary; // Class used to check the lexical correction of placed words
    private boolean wrongWord; // Boolean flag that indicates wheter the placed word is correct or wrong. Used when iterating through board fields, to either accept the placed word or to remove it from the board.
    private boolean firstLetter; // The flag that handle the special case, where the player place only one letter at the middle in the very first turn of the game. (It can't be done in Scrabble, but the program would have treated it like a correct word)
    private Ranking ranking; // Class that manages the ranking. Creates proper amount of rows and adding points to players.
    private boolean waiting = false; // Flag that indicates whether the waiting pane has been displayed. In other words dictionary wasn't loaded fast enough and user put some words on the board.
    private Label[] lettersLabelTab; // Array that holds the labels which displaying points of players' letters in the right bottom corner of the letter.
    private Label l1,l2; // Clicked labels whose will exchange points number with each other

    public Game(Controller controller) {
        this.controller = controller;
        gridPane = controller.getGridPane1();
        letterBox = controller.getLetterBox();

        lettersButtonTab = new Button[7];
        buttonTab = new Button[15][15];
        lastPressed = new Point(-1,-1);
        lastPressed2 = new Point(-1,-1);
        initializeButtons();
        initializeBoard();
        initializeDictionary();
        bag = new Bag();

        new PlayerChooser(controller,this); // First display the main menu

        activeWord = new char[15][15];
    }

    private void initializeDictionary() {
        dictionary = new DictionaryLoader(this);
    }

    // After the amount of players were chosen by user - initialize them, their letters and their ranking;
    public void initializePlayers(int players) {
        this.players = players;
        ranking = new Ranking(players, controller);

        player = new Player[players];
        for(int i=0;i<players;++i) {
            player[i]=new Player();
            player[i].letters = bag.getLetters(7);
        }
        playerLabel = controller.getPlayerLabel();

        initializeLetters();
    }

    // Initialize letters' buttons (set size, on-click event and first player letters on them). After this method, we're ready to play.
    private void initializeLetters() {
        lettersLabelTab = new Label[7];
        for(int i=0;i<7;++i) {
            Button button = new Button();
            button.setPrefSize(50.0,50.0);
            button.setOnAction(event -> letterClicked(event.getSource()));
            lettersButtonTab[i]=button;
            button.setText(player[0].getLetters()[i].getLetter()+"");

            // We are creating a new container to hold the Button (letter) and the Label (it's points)
            Pane stackPane = new Pane();
            Label label = new Label("9");

            // We need to make the label fit the parent, to enable text alignment
            label.setPrefSize(50.0,50.0);
            label.setAlignment(Pos.BOTTOM_RIGHT);
            label.setText(Integer.toString(player[0].getLetters()[i].getPoints()));
            label.setDisable(true);
            lettersLabelTab[i]=label;

            stackPane.getChildren().addAll(button,label);

            letterBox.getChildren().add(stackPane);
        }
    }

    // Initialize board and tourPane elements. We need to disable it at first.
    private void initializeBoard() {
        tourButton = controller.getTourButton();
        tourPane = controller.getTourPane();
        tourLabel = controller.getTourLabel();
        tourPane.setOpacity(0.0);
        tourPane.setDisable(true);
        tourButton.setOnAction(event -> nextPlayerReady());

        board = new Board();
    }


    // Initialize board buttons, okButton and exchangeButton.
    private void initializeButtons() {
        okButton = controller.getOkButton();
        okButton.setDisable(true);
        okButton.setOnAction(event -> wordPlaced());
        exchangeButton = controller.getExchangeButton();
        exchangeButton.setOnAction(event -> exchangeClicked());
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {

                Button button = new Button();
                button.setPrefSize(40.0, 40.0);
                button.setOpacity(0.0);
                buttonTab[i][j] = button;
                GridPane.setRowIndex(button, i);
                GridPane.setColumnIndex(button, j);


                gridPane.getChildren().add(button);
                button.setOnAction(event -> boardClicked(event.getSource()));
            }
        }
    }

    // When player decides to exchange letters. We need to change the behaviour of letters buttons and okButton as well as exchangeButton.
    private void exchangeClicked() {
        exchangeButton.setText("Do not exchange!");
        okButton.setText("Exchange!");
        for(int i=0;i<7;++i) {
            lettersButtonTab[i].setOnAction(event -> letterHighlited(event.getSource()));
        }
        exchangeButton.setOnAction(event -> goBack());
        okButton.setDisable(false);
        okButton.setOnAction(event -> exchange());
    }

    // Player decides not to exchange letters. We set normal behaviour for buttons and letters.
    private void goBack() {
        okButton.setOnAction(event -> wordPlaced());
        okButton.setDisable(true);
        okButton.setText("OK");
        exchangeButton.setOnAction(event -> exchangeClicked());
        exchangeButton.setText("Exchange letters");
        for(int i=0;i<7;++i) {
            lettersButtonTab[i].setStyle("");
            lettersButtonTab[i].setOnAction(event -> letterClicked(event.getSource()));
        }
    }

    // Player exchanged the letters. We remove them from his array of letters and draw him new ones. He lost his turn, so we call nextPlayer() method.
    private void exchange() {
        for(int i=0;i<7;++i) {
            if(lettersButtonTab[i].getStyle().equals("-fx-background-color: aquamarine")) {
                Letter newLetter = bag.getLetter();
                player[activePlayer].exchangeLetter(i,newLetter);
                lettersButtonTab[i].setText(newLetter.getLetter()+"");
                lettersButtonTab[i].setStyle("");
            }
        }
        goBack();
        tourPane.setOpacity(1.0);
        tourPane.setDisable(false);
        nextPlayer();
    }

    // Event in exchange mode to highlight clicked letters
    private void letterHighlited(Object source) {
        Button button = (Button) source;
        if(button.getStyle().equals("")) {
            button.setStyle("-fx-background-color: aquamarine");
        } else {
            button.setStyle("");
        }
    }
    // Display waiting pane, when user place the words before the dictionary was loaded
    private void displayWaitingPane() {
        waiting = true;
        tourPane.setOpacity(1.0);
        tourLabel.setText("Wait for dictionary to be loaded...");
        tourButton.setOpacity(0.0);
        tourButton.setDisable(true);
    }
    // Hide the waiting pane (when the dictionary finally loads)
    public void hideWaitingPane() {
        tourPane.setOpacity(0.0);
        tourButton.setOpacity(1.0);
        tourButton.setDisable(false);
        wordPlaced();
    }
    public boolean isWaiting() {
        return waiting;
    }
    // Player placed and accepted the word on the board. We need to collect words from Board class and then check their lexical correction.
    // If at least one word were wrong we remove it from the board and display proper info. If no, we call the method calculatePoints()
    private void wordPlaced() {
            // If dictionary is not loaded, we need to display waiting pane to the user and terminate the method.
            if(!dictionary.isDictionaryLoaded()) {
                displayWaitingPane();
                return;
            }
    wrongWord = false;
        firstLetter = false;
        board.wordPlaced(activeWord);
        words = board.getWords(activeWord);
        for (String word :
                words) {
            if (!dictionary.hasWord(word.toLowerCase())) {
                wrongWord = true;
                tourLabel.setText("There's no such a word: " + word.toLowerCase() + "! You lose your tour!");
                board.wrongWord(activeWord);
            }
        }
        if(activeWord[7][7]!='\u0000') {
            firstLetter = true;
        }
        for (int i = 0; i < 15; ++i) {
            for (int j = 0; j < 15; ++j) {
                if (activeWord[i][j] != '\u0000') {
                    activeWord[i][j] = '\u0000';
                    if(!wrongWord) {
                        buttonTab[i][j].setDisable(true);
                    } else {
                        buttonTab[i][j].setText("");
                        buttonTab[i][j].setOpacity(0.0);
                    }
                }
            }
        }
        for (int i = 0; i < 7; ++i) {
            if (lettersButtonTab[i].getOpacity() == 0.0) {
                player[activePlayer].exchangeLetter(i, bag.getLetter());
            }
        }
        okButton.setDisable(true);
        calculatePoints();
    }

    // We count points using Bag class for every word, player has placed. Also this method provides handling for the special case, when player places only one letter at the very first turn.
    private void calculatePoints() {
        int points;

        tourPane.setOpacity(1.0);
        tourPane.setDisable(false);
        points = board.getPoints(); // We get the calculated words from the baord
        tourButton.setOnAction(event -> nextPlayer());
        if(!wrongWord) {
            if(points == 0 && firstLetter) {
                char c = Character.toLowerCase(board.getBoard()[7][7].letter.getLetter());
                tourLabel.setText("No single letters in scrabble :) You lose your tour!");
                board.getBoard()[7][7].isPlaced = false;
                buttonTab[7][7].setOpacity(0.0);
                buttonTab[7][7].setDisable(false);
            } else {
                tourLabel.setText("You've scored: "+points+" points");
                ranking.addPoints(activePlayer,points);
            }
        } else {
            // No points added
        }
    }

    // The method that changes just changes the player to the next one, displaying this process on the tour pane. Now we awaits for the player to confirm his readiness.
    private void nextPlayer() {
        activePlayer = activePlayer == players-1 ? 0 : activePlayer+1;
        tourLabel.setText("Player "+(activePlayer+1));
        tourButton.setOnAction(event -> nextPlayerReady());
    }

    // Player confirmed his readiness and this method is executed. Now we disable the tour pane and prepare the next player to play (setting his letters)
    private void nextPlayerReady() {
        tourPane.setOpacity(0.0);
        tourPane.setDisable(true);
        playerLabel.setText("Player "+(activePlayer+1));
        for(int i=0;i<player[activePlayer].getLetters().length;++i) {
            Button button = lettersButtonTab[i];
            button.setText(player[activePlayer].getLetters()[i].getLetter()+"");
            Label label = lettersLabelTab[i];
            label.setText(player[activePlayer].getLetters()[i].getPoints()+"");
            button.setOpacity(1.0);
        }
        exchangeButton.setDisable(false); // Exchange button may was disabled to previous player, because he placed letters on the board.
    }

    // Event handler method, occurs when the board were clicked. We check which button was that and set lastPressed variables.
    // At the end we may exchange the letters. If some of the buttons were already clicked.
    private void boardClicked(Object o) {
        int x,y;
        for(int i=0;i<15;++i) {
            for(int j=0;j<15;++j) {
                if(o.equals(buttonTab[i][j])) {
                    x=i;y=j;
                    if(b1==null && buttonTab[i][j].getOpacity()==0.0) return;
                    if(b1==null) {
                        b1=buttonTab[i][j];
                        lastPressed.setCoords(i,j);
                    }
                    else {
                        b2=buttonTab[i][j];
                        lastPressed2.setCoords(i,j);
                    }
                    break;
                }
            }
        }
        // If board button was already clicked. We exchange on-board letter with another on-board letter;
        if(isBoardClicked) {
            exchangeLetters();
            return;
        }
        // Okay so the button we clicked is the first on-board button, so we change the flag that indicates it
        isBoardClicked = true;
        // If the letter was already clicked we exchange letter button with on-board button
        if(isLetterClicked) {
            l1.setText("");
            exchangeLetters();
        }
    }

    // Event handler method, occurs when the letter was clicked. Works similar to the board clicked method.
    private void letterClicked(Object o) {
        for(int i=0;i<7;++i) {
            if(o.equals(lettersButtonTab[i])) {
                letter1 = i;
                if(b1==null && lettersButtonTab[i].getOpacity()==0.0) return;
                if(b1==null) {
                    b1=lettersButtonTab[i];
                    l1 = lettersLabelTab[i];
                }
                else {
                    b2=lettersButtonTab[i];
                    l2 = lettersLabelTab[i];
                }
                break;
            }
        }
        // If letter vBox button was already clicked we exchange letter button with letter
        if(isLetterClicked) {
            // we also exchange the points label
            String tmp = l1.getText();
            l1.setText(l2.getText());
            l2.setText(tmp);
            exchangeLetters();
            return;
        }
        isLetterClicked = true;
        // If board was already clicked we exchange letter button with on-board button
        if(isBoardClicked) {
            l2.setText(Letter.pointsFor(b1.getText().charAt(0))+""); // We take the on-board points for letter and display it in our letters label
            exchangeLetters();
        }
    }

    // Two buttons were clicked. We exchange their letters and sets the proper opacity, that depends on what buttons were clicked - visible or not. If board is clicked, we need to
    // call the method that changes the activeWord array
    private void exchangeLetters() {
        if(isBoardClicked) wordChanged();
        isLetterClicked = false;
        isBoardClicked = false;
        // If some of the buttons are visible we change letters
        if(!(b1.getOpacity()==0.0 && b2.getOpacity()==0.0)) {
            // we need to check which button has assigned letter (may be both) and exchange them
            char c = b1.getText().length()>0 ?  b1.getText().charAt(0) :  b2.getText().charAt(0);
            b1.setText(b2.getText());
            b2.setText(c + "");
        }
        // Now we manage opacity. If buttons' opacity are opposite to each other we change them to opposite of each button's opacity. In casual words, when we place the letter on the
        // empty field.
        if(b1.getOpacity()==0.0 && b2.getOpacity()==1.0) {
            b1.setOpacity(1.0);
            b2.setOpacity(0.0);
        } else if(b1.getOpacity()==1.0 && b2.getOpacity()==0.0){
            b1.setOpacity(0.0);
            b2.setOpacity(1.0);
        }
        // Job is done. Letter exchange. We may now null the buttons and wait for another user interaction.
        b1=b2=null;
        lastPressed.setCoords(-1,-1);
        lastPressed2.setCoords(-1,-1);

        // Now we handle the enable status of exchange button. If some letter was placed on the board we need to disable it, because the move was made.
        // Only when every letter is in player HBox, he can made an exchange
        exchangeButton.setDisable(false);
        for(int i=0;i<7;++i) {
            if(lettersButtonTab[i].getOpacity()==0.0) {
                exchangeButton.setDisable(true);
            }
        }
    }
    // On-board button were clicked so we need to change the activeWord array.
    private void wordChanged() {
        int x1,y1,x2,y2;
        x1 = lastPressed.getX(); y1= lastPressed.getY();
        x2 = lastPressed2.getX(); y2= lastPressed2.getY();
        if(!isLetterClicked) {  // if board letter is exchanged with another board letter, we just simply exchange the letters in our array
            char c = activeWord[x1][y1];
            activeWord[x1][y1] = activeWord[x2][y2];
            activeWord[x2][y2] = c;
        } else { // if not
            if(x1!=-1) { // if board was clicked first (x1 is the coord of the lastPressed variable, which indicate first pressed button on the board, it is set to -1 if no button were clicked)
                if(lettersButtonTab[letter1].getText().equals("")) {    // then letter second (but the letter is not existing - we pressed the empty button)
                    activeWord[x1][y1] = '\u0000';                      // We just remove letter from our array.
                } else {    // if board clicked first then letter (letter is not blank - we pressed not empty button)
                    activeWord[x1][y1] = lettersButtonTab[letter1].getText().charAt(0); // We exchange the on-board letter with pressed letter
                }
            } else {   // if letter first board second
                activeWord[x2][y2] = lettersButtonTab[letter1].getText().charAt(0); // we just write new letter to our array
            }
        }
        // New word was placed, so we need to check it's correction of placement (in one row, next to some placed word or in the middle when no word is placed)
        if(board.isGood(activeWord)) {
            okButton.setDisable(false); // We either disable or enable the okButton whether the word is placed good or not
        } else {
            okButton.setDisable(true);
        }
    }



}
