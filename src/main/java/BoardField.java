/**
 * Created by Marcin on 2016-12-29.
 */

//Calculates the word score (holds the fields multipliers)
public class BoardField {
    Letter letter;  // Letter on the board (may be null if no letter)
    Multiplier multiplier;  // Multiplier of either letter or a word
    Boolean isPlaceAble; // Flag that indicates whether this particular board field can be a starting field of a new word, placed by a player
    Boolean isPlaced; // Flag that indicates whether letter on this field already has been placed
}
