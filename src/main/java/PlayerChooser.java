import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

/**
 * Created by Marcin on 2017-01-05.
 */
public class PlayerChooser {
    private Controller controller;
    private Pane playerPane; // The container of the main menu elements
    private Button playerButton;  // The confirmation button of the players amount
    private TextField playerTextField; // Area in which user inputs the number of players
    private Label playerLabel; // The label above the button, which informs about correct number of players
    private Game game;

    public PlayerChooser(Controller controller, Game game) {
        this.controller = controller;
        this.game = game;
        initializeElements();
    }
    // Initialize elements of the main menu and the listener to the button
    private void initializeElements() {
        playerButton = controller.getPlayerButton();
        playerButton.setOnAction(event -> onPlay());

        playerTextField = controller.getPlayerTextField();

        playerLabel = controller.getMenuPlayerLabel();

        playerPane = controller.getPlayerPane();
    }

    // When the button was pressed we check the validity of the entered value. If valid we display the board and call initializePlayers(int) method from the game class
    private void onPlay() {
        String input = playerTextField.getText();
        int result;
        try {
            result = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            playerLabel.setText("Enter valid number! (from 2 to 4)");
            return;
        }
        if(result>4 || result <2) {
            playerLabel.setText("Enter value between 2 and 4:");
            return;
        }
        playerPane.setDisable(true);
        playerPane.setOpacity(0.0);

        game.initializePlayers(result);
    }


}
