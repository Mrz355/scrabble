/**
 * Created by Marcin on 2016-12-27.
 */
public class Player {
    Letter[] letters;

    public Letter[] getLetters() {
        return letters;
    }

    public void exchangeLetter(int i, Letter letter) {
        letters[i] = letter;
    }
}
