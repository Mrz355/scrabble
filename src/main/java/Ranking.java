import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

/**
 * Created by Marcin on 2017-01-05.
 */
public class Ranking {

    private int players;  // Number of all players in the game
    private Controller controller;
    private int[] points;  // Array holding points of every player
    private GridPane gridRanking; // The grid container of ranking, displaying the number of players and their points in other rows
    private Label[] pointsLabels; // This array holds pointsLabels, which indicates the particular player points

    public Ranking(int players, Controller controller) {
        this.players = players;
        this.controller = controller;
        points = new int[players];
        pointsLabels = new Label[players];
        for(int i=0;i<players;++i) points[i]=0;
        initializeRanking();
    }

    private void initializeRanking() {
        gridRanking = controller.getGridRanking();
        for(int i=0;i<players;++i) {
            Label playerLabel = new Label("Player "+(i+1));
            Label pointsLabel = new Label("0");
            //Adding nice padding to labels
            playerLabel.setStyle("-fx-padding: 10px");
            pointsLabel.setStyle("-fx-padding: 10px");
            pointsLabels[i]=pointsLabel;
            GridPane.setColumnIndex(pointsLabel,1);
            GridPane.setRowIndex(pointsLabel,i);
            GridPane.setColumnIndex(playerLabel,0);
            GridPane.setRowIndex(playerLabel,i);

            //Adding label to a new row
            gridRanking.getChildren().add(playerLabel);
            gridRanking.getChildren().add(pointsLabel);
        }
    }

    public void addPoints(int player, int amount) {
        points[player] += amount;
        pointsLabels[player].setText(Integer.toString(points[player]));

        // Sample wining condition
//        if(points[player] >= 150/players) {
//            controller.getTourPane().setOpacity(1.0);
//            controller.getTourLabel().setText("Player "+(player+1)+" win!");
//            controller.getTourButton().setText("Exit");
//            controller.getTourButton().setOnAction(event-> System.exit(0));
//        }
    }

}
