import java.util.LinkedList;
import java.util.Random;

/**
 * Created by Marcin on 2016-12-26.
 */
public class Bag {

    public LinkedList<Letter> bag;
    private Random r;

    public Bag() {

        bag = new LinkedList<>();
        r = new Random();
        createAlphabet();
    }

    public Letter[] getLetters(int n) {
        Letter[] a = new Letter[n];

        for (int i = 0;i<n;++i) {
            int x = r.nextInt(bag.size());
            a[i] = bag.get(x);
        }

        return a;
    }
    public Letter getLetter() {
        int index = r.nextInt(bag.size());
        Letter letter = bag.get(index);
        return letter;
    }

    private void createAlphabet() {
        addLetter('A',9);
        addLetter('Ą');
        addLetter('B',2);
        addLetter('C',3);
        addLetter('Ć');
        addLetter('D',3);
        addLetter('E',7);
        addLetter('Ę');
        addLetter('F');
        addLetter('G',2);
        addLetter('H',2);
        addLetter('I',8);
        addLetter('J',2);
        addLetter('K',3);
        addLetter('L',3);
        addLetter('Ł',2);
        addLetter('M',3);
        addLetter('N',5);
        addLetter('Ń');
        addLetter('O',6);
        addLetter('Ó');
        addLetter('P',3);
        addLetter('R',4);
        addLetter('S',4);
        addLetter('Ś');
        addLetter('T',3);
        addLetter('U',2);
        addLetter('W',4);
        addLetter('Y',4);
        addLetter('Z',5);
        addLetter('Ź');
        addLetter('Ż');
    }

    private void addLetter(char letter) {
        bag.add(new Letter(letter));
    }
    private void addLetter(char letter, int howMany) {
        for(int i=0;i<howMany;++i) {
            addLetter(letter);
        }
    }
}
